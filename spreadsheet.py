import csv


def write_to_file(name, city, email):
    file = open("registrants.csv", "a")
    writer = csv.writer(file)
    writer.writerow((name, city, email))
    file.close()


def read_from_file():
    list = []
    with open('registrants.csv', newline='') as file:
        reader = csv.reader(file)
        for row in reader:
            print(row)
            list.append(row)
    return list
