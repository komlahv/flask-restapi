# flask run to run server
from flask import Flask, render_template, request, redirect
from flask_cors import CORS
from mail import send_mail
from spreadsheet import write_to_file, read_from_file

app = Flask(__name__)
CORS(app)


@app.route("/")
def index():
    students = read_from_file()
    return render_template(
        "index.html",
        students=students

    )


@app.route("/register", methods=["POST"])
def register():
    name = request.form.get("name")
    city = request.form.get("city")
    email = request.form.get("email")
    if not name or not city:
        return render_template("failure.html")
    # students.append(f"{name} from {city}")
    write_to_file(name, city, email)
    send_mail(email)
    return redirect("/success")


@app.route("/success")
def success():
    return render_template("success.html")
